#include <iostream>
#include <cstdlib>
#include <string>
#include "HashTable_chains.h"
#include "HeshTable_open_addressing.h"

using namespace std;

HTC::HashTable<string> htc;
HTOA::HashTable<string> htoa;

int main()
{
    int n = 1000;
    for(int i = 0; i<n; i++){
        unsigned key = rand()%1000;
        string value = "value ";
        value += key%100;
        htc.set(key, value);
    }
    htc.output();
    cout<<"****************\n";

    int q = 128;
    for(int i = 0; i<q; i++){
        unsigned key = rand()%1000;
        string value = "value ";
        value += key%100;
        htoa.set(key, value);
    }

    /*htc.set(0,"key 0").set(10,"key 10").set(128,"key 128");*/
    /*htoa.set(0,"key 0").set(10,"key 10").set(128,"key 128");

    htoa.output();

    cout<<"****************\n"<<htoa[10]<<"\n";
    htoa.remove(10);*/

    htoa.output();
    return 0;
}
