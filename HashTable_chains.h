#include <list>
#include <cmath>
#include <iostream>
#include <cstdio>

#ifndef HASHTABLE_CHAINS_H
#define HASHTABLE_CHAINS_H

namespace HTC {


template <typename TValue>
class HashTable
{
    public:
        HashTable();
        virtual ~HashTable();
        HashTable& set(unsigned key, TValue value);
        HashTable& remove(unsigned key);
        TValue operator[](unsigned key) const;
        void output() const;
    private:
        struct data {
            unsigned key;
            TValue value;
            data(unsigned k, TValue v): key(k), value(v) {}
        };
        static unsigned const SIZE = 128;
        std::list<data> *arr[SIZE];
        static unsigned getHashKey(unsigned key);
        HashTable::data *search(unsigned key) const;
};

template<typename TValue>
HashTable<TValue>& HashTable<TValue>::remove(unsigned key){
    unsigned k = getHashKey(key);
    typename std::list<data>::iterator iter;

    if(arr[k]){
        for(iter = arr[k]->begin(); iter != arr[k]->end(); iter++){
            if( (*iter).key == key ) {
                arr[k]->erase(iter);
                break;
            }
        }

        if(arr[k]->empty()){
            delete arr[k];
            arr[k] = NULL;
        }
    }

    return *this;
}

template<typename TValue>
void HashTable<TValue>::output() const {
    unsigned n = 0;
    typename std::list<data>::iterator iter;
    for (unsigned i=0; i<SIZE; i++){
        if(arr[i]){
            for(iter = arr[i]->begin(); iter != arr[i]->end(); iter++){
                std::cout<<(*iter).key<<"->"<<(*iter).value<<"\t";
                n++;
            }
            std::cout<<"\n";
        }
    }
    printf("number items: %i\n", n);
    printf("filling factor: %1.5f\n", ( (double)n )/( (double)SIZE ) );
}

template<typename TValue>
TValue HashTable<TValue>::operator[](unsigned key) const {
    TValue answer;
    data *item = search(key);

    if(!item) throw "not found";

    return item->value;
}

template<typename TValue>
typename HashTable<TValue>::data *HashTable<TValue>::search(unsigned key) const {
    unsigned k = getHashKey(key);
    data *answer = NULL;
    typename std::list<data>::iterator iter;

    if(arr[k]){
        for(iter = arr[k]->begin(); iter != arr[k]->end(); iter++){
            if( (*iter).key == key ) {
                answer = &(*iter);
            }
        }
    }

    return answer;
}

template<typename TValue>
HashTable<TValue>& HashTable<TValue>::set(unsigned key, TValue value){
    data *item;
    item = search(key);
    if(item){
        item->value = value;
        return *this;
    }

    unsigned k = getHashKey(key);
    if(!arr[k]){
        arr[k] = new std::list<data>;
    }

    item = new data(key, value);
    arr[k]->push_front(*item);

    return *this;
}

template<typename TValue>
/*unsigned HashTable<TValue>::getHashKey(unsigned key) {
    return (unsigned)key%SIZE;
}*/
unsigned HashTable<TValue>::getHashKey(unsigned key) {
    return std::ceil( SIZE*std::fmod( key*(std::sqrt(5)-1)/2, 1 ) );
}

template<typename TValue>
HashTable<TValue>::HashTable() {
    for(unsigned i=0; i<SIZE; i++){
        arr[i] = NULL;
    }
}

template<typename TValue>
HashTable<TValue>::~HashTable() {
    for(unsigned i=0; i<SIZE; i++){
        if(arr[i]){
            delete arr[i];
            arr[i] = NULL;
        }
    }
}

}

#endif // HASHTABLE_CHAINS_H
