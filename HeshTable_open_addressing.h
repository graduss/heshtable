#include <iostream>
#include <cstdio>
#include <cmath>

#ifndef HESHTABLE_OPEN_ADDRESSING_H
#define HESHTABLE_OPEN_ADDRESSING_H

namespace HTOA {

template <typename TValue>
class HashTable
{
    public:
        HashTable();
        virtual ~HashTable();
        HashTable& set(unsigned key, TValue value);
        HashTable& remove(unsigned key);
        TValue operator[](unsigned key) const;
        void output() const;
    private:
        static unsigned const SIZE = 128;
        struct data {
            unsigned key;
            TValue value;
            bool del;
            data(unsigned k, TValue v): key(k), value(v), del(false) {}
        } *arr[SIZE];
        static unsigned getHashKey(unsigned key, unsigned i);
};

template<typename TValue>
void HashTable<TValue>::output() const {
    unsigned n = 0;
    unsigned colums = 5;
    unsigned col = 0;

    for(unsigned i = 0; i<SIZE; i++){
        if(arr[i] && !arr[i]->del){
            std::cout<<arr[i]->key<<"->"<<arr[i]->value;
            n++;
            if(col<colums){
                std::cout<<"\t";
                col++;
            }else{
                std::cout<<"\n";
                col = 0;
            }
        }
    }

    if(col != 0) std::cout<<"\n";
    std::cout<<"number items: "<<n<<"\n";
    printf("filling factor: %1.5f \n", ( (double)n )/( (double)SIZE ) );
}

template<typename TValue>
TValue HashTable<TValue>::operator[](unsigned key) const {
    unsigned k;
    unsigned i = 0;
    do {
        k = getHashKey(key, i);
        i++;
        if(arr[k]){
            if(!arr[k]->del && arr[k]-> key == key){
                return arr[k]->value;
            }
        }else{
            throw "not found";
        }
    }while(i<SIZE);

    throw "not found";
}

template<typename TValue>
HashTable<TValue>& HashTable<TValue>::remove(unsigned key){
    unsigned k;
    unsigned i = 0;
    do {
        k = getHashKey(key, i);
        if(arr[k]){
            if(arr[k]->key == key){
                arr[k]->del = true;
                return *this;
            }
        }else{
            throw "not found";
        }
        i++;
    }while(i<SIZE);

    throw "not found";
}

template<typename TValue>
HashTable<TValue>& HashTable<TValue>::set(unsigned key, TValue value){
    unsigned k;
    unsigned i = 0;
    do {
        k = getHashKey(key,i);
        if(arr[k] == NULL ){
            arr[k] = new data(key,value);
            return *this;
        }else if(arr[k]->del){
            arr[k]->key = key;
            arr[k]->value = value;
            arr[k]->del = false;
            return *this;
        }
        i++;
    }while(i<SIZE);
    throw "overflow";
}

template<typename TValue>
unsigned HashTable<TValue>::getHashKey(unsigned key, unsigned i) {
    return (unsigned)((key%SIZE) + i)%SIZE;
}
/*unsigned HashTable<TValue>::getHashKey(unsigned key, unsigned i) {
    unsigned c1 = 0;
    unsigned c2 = 1;

    return (unsigned) ((key%SIZE) + c1*i + c2*std::pow(i,2) )%SIZE;
}*/

template<typename TValue>
HashTable<TValue>::HashTable(){
    for(unsigned i = 0; i<SIZE; i++){
        arr[i] = NULL;
    }
}

template<typename TValue>
HashTable<TValue>::~HashTable(){
    for(unsigned i = 0; i<SIZE; i++){
        if(arr[i]){
            delete arr[i];
            arr[i] = NULL;
        }
    }
}

}

#endif // HESHTABLE_OPEN_ADDRESSING_H
